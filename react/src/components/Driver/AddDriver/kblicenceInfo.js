import React, { Component } from "react";
import { Form, FormGroup, ControlLabel, FormControl, Button } from "react-bootstrap";
import PropTypes from "prop-types";
import request from 'superagent';
import { FormattedMessage } from "react-intl";
import "../../../styles/common/add_driver.scss";
import KBlicenceInfoForm from "./kbLicenceInfoForm";

class KBLIcenceInfo extends Component {
    static propTypes = {
        licenceobj: PropTypes.object,
        onUpdateLicenceInfo: PropTypes.func
    };
    constructor(props) {
        super(props);
        this.state = {
            mm: null,
            dd: null,
            yyyy: null,
            KBlicenceDocuments: {
                KBlicenceUrl: null,
            },
            KBlicenceDetails: {
                KBlicenceNo: null,
                KBreleaseDate: null,
                KBexpDate: null,
                KBrevisalDate: null,
            },
            file1: '',
            imagePreviewUrl: '',
        };
    }
    handleChange(inputname, value, label) {
        if (inputname === "KBmonth" || inputname === "KBemonth" || inputname === "KBrmonth") {
            this.setState({ mm: value });
            const date = `${this.state.mm}/${this.state.dd}/${this.state.yyyy}`;
        } else if (inputname === "KBdate" || inputname === "KBedate" || inputname === "KBrdate") {
            this.setState({ dd: value });
            const date = `${this.state.mm}/${this.state.dd}/${this.state.yyyy}`;
        } else if (inputname === "KByear" || inputname === "KBeyear" || inputname === "KBryear") {
            this.setState({ yyyy: value });
            const date = `${this.state.mm}/${this.state.dd}/${value}`;

            if (label === "Id") {
                this.state.KBlicenceDetails["KBreleaseDate"] = date
            }
            if (label === "Ed") {
                this.state.KBlicenceDetails["KBexpDate"] = date
            }
            if (label == "Rd") {
                this.state.KBlicenceDetails["KBrevisalDate"] = date
            }

            this.props.onUpdateLicenceInfo(
                "KBlicenceDetails",
                this.state.KBlicenceDetails
            );
        } else {
            this.state.KBlicenceDetails["KBlicenceNo"] = value;
            this.props.onUpdateLicenceInfo(
                "KBlicenceDetails",
                this.state.KBlicenceDetails
            );
        }
    }
    handleIssueDate(label) {
        const date = `${this.state.Imm}/${this.state.Idd}/${this.state.Iyyyy}`;
        this.props.onUpdateLicenceInfo(date, label, true);
    }
    handleExpDate(label) {
        const date = `${this.state.Emm}/${this.state.Edd}/${this.state.Eyyyy}`;
        this.props.onUpdateLicenceInfo(date, label, true);
    }

    _handleImageChange(e, type) {
        e.preventDefault();
        var fileTypes = ['jpg', 'jpeg', 'png'];
        if (e.target.files[0]) {
            var extension = e.target.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                isSuccess = fileTypes.indexOf(extension) > -1;
            if (isSuccess) {
                let reader = new FileReader();
                let file = e.target.files[0];

                reader.onloadend = () => {
                    this.setState({
                        file1: file,
                        imagePreviewUrl: reader.result
                    }),
                        this.handleImageUploadcloudinary(reader.result, type)
                }
                reader.readAsDataURL(file)
            } else {
                this.setState({
                    file1: '',
                    imagePreviewUrl: ''
                })

                alert("only '.jpg' , '.jpeg' , '.png' file types are accepted");
            }
        } else {
            this.setState({
                file1: '',
                imagePreviewUrl: ''
            })
        }
    }

    handleImageUploadcloudinary(file, type) {
        let upload = request.post('https://api.cloudinary.com/v1_1/taxiapp1/upload')
            .field('upload_preset', "bkfchx7x")
            .field('file', file);

        upload.end((err, response) => {
            if (err) {
                console.error(err);
            }
            if (response.body.secure_url !== '') {
                this.state.KBlicenceDocuments['KBlicenceUrl'] = response.body.secure_url,
                    this.props.onUpdateLicenceInfo("KBlicenceDocuments", this.state.KBlicenceDocuments)
            }
        });
    }

    render() {
        return (
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className="panel panel-primary adddriverpanel">
                    <div className="panel-heading">
                        <span>
                            {" "}
                            <FormattedMessage
                                id={"kb_license_info"}
                                defaultMessage={"KB License Info"}
                            />
                        </span>
                    </div>
                    <div className="panel-body">
                        <div className="col-sm-12 col-md-6 col-lg-6">
                            <KBlicenceInfoForm
                                OnformBlur={(name, val, label) =>
                                    this.handleChange(name, val, label)
                                }
                            />
                        </div>

                        <div className="col-sm-12 col-md-6 col-lg-6">
                            <div className="previewComponent">
                                <Form>
                                    <div className="form-group">
                                        <label>KB Licence Certificate:</label>
                                        <input className="form-control"
                                            type="file"
                                            onChange={(e) => this._handleImageChange(e, 'kblicence')} />
                                    </div>
                                    <Button style={{ border: 'none' }} />
                                </Form>
                                <div className="form-group">
                                    <div className="imgPreview col-sm-12">
                                        {
                                            this.state.imagePreviewUrl ?
                                                <img alt="Insurance" src={this.state.imagePreviewUrl} />
                                                :
                                                <div className="previewText">Please select an Image for Preview</div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default KBLIcenceInfo;
