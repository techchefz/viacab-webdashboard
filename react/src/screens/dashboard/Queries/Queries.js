import React, { Component } from 'react';
import { connect } from "react-redux";
import moment from "moment";
import _ from "lodash";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { Table, Button, Tabs, Tab } from "react-bootstrap";
import QueryAction from '../../../redux/queries/action';
import QueryDetails from '../../../components/Queries';

class Queries extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchTerm: "",
            queryList: null,
            searchType: "MOBILE",
            openDetails: false,
            selectedQuery: null,
        }
    }

    componentWillMount = () => {
        this.props.fetchQueries();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.queriesList !== null) {
            this.setState({
                ...this.state,
                queryList: nextProps.queriesList.data
            })
        }
    }

    filteredProducts(searchTerm) {
        const queryList = this.state.queryList;
        let list = null;

        if (queryList !== undefined && searchTerm !== '' && searchTerm.length > 1) {
            if (this.state.searchType === "COMPLAINT_ID") {
                list = queryList.filter((query) => query._id.indexOf(searchTerm) !== -1);

            }
            else if (this.state.searchType === "MOBILE") {
                list = queryList.filter((query) => query.userId.phoneNo.indexOf(searchTerm) !== -1);
            }

            this.state.queryList = list;
        } else if (searchTerm === '' || searchTerm.length <= 1) {
            this.state.queryList = this.props.queriesList.data;
        }
    }


    changeQueryStatus(changedStatus) {
        const queryObj = {
            tripId: this.state.selectedQuery._id,
            queryStatus: changedStatus
        }
        let ask = window.confirm(`Are you sure you want to set it ${changedStatus}`);
        if (ask === true) {
            this.props.changeQueryStatus(queryObj);
            this.setState({ openDetails: false });
            window.location.reload();
        }
    }



    render() {
        return (
            <div className="panel panel-primary">
                <div className="panel-heading panelheading">
                    <span>

                        {" "}
                        <FormattedMessage
                            id={"user_queries"}
                            defaultMessage={"USER QUERIES"}
                        />
                    </span>
                    {this.state.openDetails === false ?
                        <div className="search-box">
                            <input
                                type="text"
                                className="form-control"
                                placeholder={`SEARCH QUERY BY ${this.state.searchType}`}
                                value={this.state.searchTerm}
                                onChange={(event) => {
                                    this.setState({ searchTerm: event.target.value });
                                    this.filteredProducts(this.state.searchTerm);
                                }}

                            />
                            <select className="form-control" onChange={(event) => this.setState({ searchType: event.target.value })}>
                                <option >MOBILE</option>
                                <option >E-MAIL</option>
                            </select>
                        </div>
                        :
                        <div style={{
                            float: "right",
                            marginTop: -7,
                            marginRight: -10,
                            display: "block"
                        }}>
                            <Button onClick={() => {
                                this.setState({ openDetails: false, selectedRide: null });
                            }}>CLOSE</Button>
                        </div>}
                </div>
                <div className="panel-body panelTableBody">
                    {this.state.openDetails === false ?
                        <Tabs
                            defaultActiveKey={1}
                            animation={false}
                            id="noanim-tab-example"
                            className="col-lg-12 col-md-12 col-sm-12 col-xs-12"
                            style={{ float: "left" }}
                        >
                            <Tab eventKey={1} title="ACTIVE">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div className="table-responsive">
                                            <table className="col-xs-12 panelTable">
                                                <thead>
                                                    <tr className="panelTableHead">
                                                        <th className="col-md-1">
                                                            {" "}
                                                            <FormattedMessage id={"queryID"}
                                                                defaultMessage={"ID"} />
                                                        </th>
                                                        <th className="col-md-2">
                                                            <FormattedMessage id={"status"}
                                                                defaultMessage={"status"} />
                                                        </th>
                                                        <th className="col-md-2">
                                                            {" "}
                                                            <FormattedMessage
                                                                id={"riderName"}
                                                                defaultMessage={"Name"}
                                                            />
                                                        </th>
                                                        <th className="col-md-4">
                                                            {" "}
                                                            <FormattedMessage
                                                                id={"Subject"}
                                                                defaultMessage={"Subject"}
                                                            />
                                                        </th>
                                                        <th className="col-md-2">
                                                            {" "}
                                                            <FormattedMessage
                                                                id={"date"}
                                                                defaultMessage={"Date"}
                                                            />
                                                        </th>
                                                        <th className="col-md-1">
                                                            {" "}
                                                            <FormattedMessage
                                                                id={"time"}
                                                                defaultMessage={"Time"}
                                                            />
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody className="panelTableTBody">
                                                    {
                                                        this.state.queryList !== null ? this.state.queryList.map((item, index) =>
                                                            item.queryStatus !== "Resolved" ? (
                                                                <tr key={index}
                                                                    onClick={() => {
                                                                        this.setState({ openDetails: true, selectedQuery: item });
                                                                    }}>
                                                                    <td>{_.get(item, "_id", "NA")}</td>
                                                                    <td>{_.get(item, "queryStatus", "NA")}</td>
                                                                    <td>{_.get(item.userId, "fname", "")} {_.get(item.userId, "lname", "NA")}</td>
                                                                    <td>{_.get(item, "querySubject", "NA")}</td>
                                                                    <td>{moment(_.get(item, "submittedOn", "NA")).format("DD MMM YYYY")}</td>
                                                                    <td>{moment(_.get(item, "submittedOn", "NA")).format(" h:mm a")}</td>
                                                                </tr>) : null
                                                        ) : <p>No Queries Found</p>
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </Tab>
                            <Tab eventKey={2} title="RESOLVED">
                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div className="table-responsive">
                                            <table className="col-xs-12 panelTable">
                                                <thead>
                                                    <tr className="panelTableHead">
                                                        <th className="col-md-1">
                                                            {" "}
                                                            <FormattedMessage id={"queryID"}
                                                                defaultMessage={"ID"} />
                                                        </th>
                                                        <th className="col-md-2">
                                                            <FormattedMessage id={"status"}
                                                                defaultMessage={"status"} />
                                                        </th>
                                                        <th className="col-md-2">
                                                            {" "}
                                                            <FormattedMessage
                                                                id={"riderName"}
                                                                defaultMessage={"Name"}
                                                            />
                                                        </th>
                                                        <th className="col-md-4">
                                                            {" "}
                                                            <FormattedMessage
                                                                id={"Subject"}
                                                                defaultMessage={"Subject"}
                                                            />
                                                        </th>
                                                        <th className="col-md-2">
                                                            {" "}
                                                            <FormattedMessage
                                                                id={"date"}
                                                                defaultMessage={"Date"}
                                                            />
                                                        </th>
                                                        <th className="col-md-1">
                                                            {" "}
                                                            <FormattedMessage
                                                                id={"time"}
                                                                defaultMessage={"Time"}
                                                            />
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody className="panelTableTBody">
                                                    {
                                                        this.state.queryList !== null ? this.state.queryList.map((item, index) =>
                                                            item.queryStatus === "Resolved" ? (
                                                                <tr key={index}
                                                                    onClick={() => {
                                                                        this.setState({ openDetails: true, selectedQuery: item });
                                                                    }}>
                                                                    <td>{_.get(item, "_id", "NA")}</td>
                                                                    <td>{_.get(item, "queryStatus", "NA")}</td>
                                                                    <td>{_.get(item.userId, "fname", "")} {_.get(item.userId, "lname", "NA")}</td>
                                                                    <td>{_.get(item, "querySubject", "NA")}</td>
                                                                    <td>{moment(_.get(item, "submittedOn", "NA")).format("DD MMM YYYY")}</td>
                                                                    <td>{moment(_.get(item, "submittedOn", "NA")).format(" h:mm a")}</td>
                                                                </tr>) : null
                                                        ) : <p>No Queries Found</p>
                                                    }
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </Tab>
                        </Tabs> :
                        <div className="col-xs-12 panelTable"><QueryDetails queryObj={this.state.selectedQuery} changeQueryStatus={this.changeQueryStatus.bind(this)} /></div>}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        queriesList: state.userQueries.userQueries,
    };
}
function bindActions(dispatch) {
    return {
        fetchQueries: () =>
            dispatch(QueryAction.fetchUserQueries()),
        changeQueryStatus: (payload) =>
            dispatch(QueryAction.updateUserQueries(payload))
    };
}

export default connect(mapStateToProps, bindActions)(Queries)
