export default {
  serverUrl: "http://192.168.0.3",
  // serverUrl: "http://159.89.3.128", // use Ip address instead of localhost
  port: 3000 // incase of running api-server (node app) in development
  // port: 3066 //incase of running api-server (node app) in production
  //  port: 443 //incase of running api-server (node app) on heroku
};
