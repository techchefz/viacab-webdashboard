import React, { Component } from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { FormattedMessage } from "react-intl";
import "../../../styles/common/add_driver.scss";

const validate = values => {
    const errors = {};
    if (!values.KBlicenceNo) {
        errors.KBlicenceNo = "Required";
    }
    if (!values.KBdate) {
        errors.KBdate = "Required";
    }
    if (!values.KBmonth) {
        errors.KBmonth = "Required";
    }
    if (!values.KByear) {
        errors.KByear = "Required";
    }
    if (!values.KBedate) {
        errors.KBedate = "Required";
    }
    if (!values.KBemonth) {
        errors.KBemonth = "Required";
    }
    if (!values.KBeyear) {
        errors.KBeyear = "Required";
    }
    if (!values.KBrdate) {
        errors.KBrdate = "Required";
    }
    if (!values.KBrmonth) {
        errors.KBrmonth = "Required";
    }
    if (!values.KBryear) {
        errors.KBryear = "Required";
    }
    return errors;
};

const renderField = ({
    input,
    label,
    placeholder,
    type,
    className,
    OnformBlur,
    meta: { touched, error, warning }
}) => (
        <div>
            {label === "Id" || label === "Ed" || label === "Rd" ? (
                <span style={{ width: 0 }} />
            ) : (
                    <label className="col-md-4 col-lg-4 col-sm-4 formlabel"> {label}</label>
                )}
            <div>
                <input
                    className={className}
                    {...input}
                    type={type}
                    placeholder={placeholder}
                    onBlur={(e) => {
                        input.onBlur(e);
                        OnformBlur(input.name, input.value, label);
                    }}
                />
                {touched &&
                    ((error && (
                        <span
                            style={{
                                color: "red",
                                textAlign: "center",
                                display: "block"
                            }}
                        >
                            {error}
                        </span>
                    )))}
            </div>
        </div>
    );

class KBlicenceInfoForm extends Component {
    static propTypes = {
        dispatch: PropTypes.func,
        handleSubmit: PropTypes.func,
        OnformBlur: PropTypes.func
    };
    constructor(props) {
        super(props);
        this.state = {
            isfetched: false
        };
    }
    render() {
        return (
            <form className="form">
                <div className="col-md-12 col-lg-12">
                    <div className="form-group">
                        <label>KB License #</label>
                        <Field
                            className="col-md-8 col-lg-8 col-sm-8 form-control"
                            name="KBlicenceNo"
                            component={renderField}
                            type="text"
                            placeholder="Licence No"
                            OnformBlur={this.props.OnformBlur}
                        />
                    </div>
                    <div className="form-group">
                        <label>KB Issue Date</label>
                        <div>
                            <div className="col-sm-4 col-xs-4">
                                <Field
                                    className="col-md-4 col-lg-4 col-sm-4 form-control"
                                    name="KBmonth"
                                    label="Id"
                                    component={renderField}
                                    placeholder="MM"
                                    OnformBlur={this.props.OnformBlur}
                                />
                            </div>
                            <div className="col-sm-4 col-xs-4">
                                <Field
                                    className="col-md-4 col-lg-4 col-sm-4 form-control"
                                    name="KBdate"
                                    label="Id"
                                    component={renderField}
                                    placeholder="DD"
                                    OnformBlur={this.props.OnformBlur}
                                />
                            </div>
                            <div className="col-sm-4 col-xs-4">
                                <Field
                                    className="col-md-4 col-lg-4 col-sm-4 form-control"
                                    name="KByear"
                                    label="Id"
                                    component={renderField}
                                    placeholder="YYYY"
                                    OnformBlur={this.props.OnformBlur}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <label>KB Exp Date</label>
                        <div>
                            <div className="col-sm-4 col-xs-4">
                                <Field
                                    className="col-md-4 col-lg-4 col-sm-4 form-control"
                                    name="KBemonth"
                                    label="Ed"
                                    component={renderField}
                                    placeholder="MM"
                                    OnformBlur={this.props.OnformBlur}
                                />
                            </div>
                            <div className="col-sm-4 col-xs-4">
                                <Field
                                    className="col-md-4 col-lg-4 col-sm-4 form-control"
                                    name="KBedate"
                                    label="Ed"
                                    component={renderField}
                                    placeholder="DD"
                                    OnformBlur={this.props.OnformBlur}
                                />
                            </div>
                            <div className="col-sm-4 col-xs-4">
                                <Field
                                    className="col-md-4 col-lg-4 col-sm-4 form-control"
                                    name="KBeyear"
                                    label="Ed"
                                    component={renderField}
                                    placeholder="YYYY"
                                    OnformBlur={this.props.OnformBlur}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <label>KB Revisal Date</label>
                        <div>
                            <div className="col-sm-4 col-xs-4">
                                <Field
                                    className="col-md-4 col-lg-4 col-sm-4 form-control"
                                    name="KBrmonth"
                                    label="Rd"
                                    component={renderField}
                                    placeholder="MM"
                                    OnformBlur={this.props.OnformBlur}
                                />
                            </div>
                            <div className="col-sm-4 col-xs-4">
                                <Field
                                    className="col-md-4 col-lg-4 col-sm-4 form-control"
                                    name="KBrdate"
                                    label="Rd"
                                    component={renderField}
                                    placeholder="DD"
                                    OnformBlur={this.props.OnformBlur}
                                />
                            </div>
                            <div className="col-sm-4 col-xs-4">
                                <Field
                                    className="col-md-4 col-lg-4 col-sm-4 form-control"
                                    name="KBryear"
                                    label="Rd"
                                    component={renderField}
                                    placeholder="YYYY"
                                    OnformBlur={this.props.OnformBlur}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: "KBlicencedriver", // a unique identifier for this form
    validate, // <--- validation function given to redux-form
})(KBlicenceInfoForm);
